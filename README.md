Intro
- 

Before you start verify it, read this!

Be patient and honest. Respect my and your time. 
Task was completed with minimum meaningful requirements. We can rewrite everything, test everything, but for what. This is our time.

I spent around 5 hours for that. You can check it with history commit.

[Task requirements](https://bitbucket.org/Kami-sama/conducttask/downloads/kyiv-javaengineertesttask-220518-0828-22.pdf)

Installation and Startup
-

For startup apps you need download 2 artifacts: [server](https://bitbucket.org/Kami-sama/conducttask/downloads/ConductServer-1.0-SNAPSHOT-boot.jar) and [client](https://bitbucket.org/Kami-sama/conducttask/downloads/ConductClient-1.0-SNAPSHOT-boot.jar)

For start server, please type in your terminal:
```
java -jar ConductServer-1.0-SNAPSHOT-boot.jar
```
For start client:
```
java -jar /ConductClient-1.0-SNAPSHOT-boot.jar
```
Server will run on **8080 Port.**
Client will try occupied **8090 Port.**

If you wanna change ports, please clarify system properties on startup:  
```
java -jar ConductServer-1.0-SNAPSHOT-boot.jar -Dserver.port=9000
```

Improvements
-

Things what can be improved:
* Gradle dependencies between modules
* Add more logs and clear logs
* Add Spring MVC test at least for Server module, not sure that it needed for client for such MVP
* Create common jar for API contract, but also we have simple contract for now
* Create contract tests 
* Improve structure and algorithm for storage and search documents 
 
 
Client API
-
 
#### Add Document
 
 ``http://localhost:8090/addDocument?key=word2&value=My+qwe+mom``
 
 _key_ and _value_ of document what you like to add.
 
#### Get Document
 ``http://localhost:8090/document?key=word2``
 
#### Search request
 ``http://localhost:8090/searchKeys?tokens=mom,qwe``
 
 **tokens** - list of words for searching inside document, keys list will be returned
 _Use comma delimiter._
 