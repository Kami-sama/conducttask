package com.conduct.repository.impl;

import com.conduct.server.repository.DocumentRepository;
import com.conduct.server.repository.impl.InMemoryDocumentRepositoryImpl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class InMemoryDocumentRepositoryImplTest {

    private static final String KEY_1 = "key1";
    private static final String KEY_2 = "key2";
    private static final String KEY_3 = "key3";
    private static final String UNIQUE_KEY = "unique_key";
    private static final String KEY_5 = "key5";
    private static final String VALUE_1 = "value1";
    private static final String NEW_KEY = "NEW KEY";
    private static final String NEW_VALUE = "NEW VALUE";
    private static final String UNIQUE_VALUE = "unique_value";

    private DocumentRepository repository;
    private Map<String, String> map;

    @Before
    public void setUp() throws Exception {
        repository = new InMemoryDocumentRepositoryImpl();
        map = new ConcurrentHashMap<>();
        map.put(KEY_1, VALUE_1);
        map.put(KEY_2, "value1 value2 value3");
        map.put(KEY_3, "value1 value3 value4 value5");
        map.put(UNIQUE_KEY, UNIQUE_VALUE);
        map.put(KEY_5, "value1 value2");

        ReflectionTestUtils.setField(repository, "documentCache", map);
    }

    @Test
    public void getDocumentByKey() {
        assertEquals(VALUE_1, repository.getDocumentByKey(KEY_1));
    }

    @Test
    public void returnNullWhenTriedGetDocumentWithEmptyKey() {
        assertNull(repository.getDocumentByKey(""));
    }

    @Test
    public void returnNullWhenTriedGetDocumentWithNullKey() {
        assertNull(repository.getDocumentByKey(null));
    }

    @Test
    public void addNewDocument() {
        repository.addDocument(NEW_KEY, NEW_VALUE);
        assertEquals(NEW_VALUE, map.get(NEW_KEY));
    }

    @Test
    public void returnNullWhenTriedAddDocumentWithEmptyKey() {
        String document = repository.addDocument("", NEW_VALUE);
        assertNull(document);
    }

    @Test
    public void returnNullWhenTriedAddDocumentWithNullKey() {
        String document = repository.addDocument(null, NEW_VALUE);
        assertNull(document);
    }

    @Test
    public void returnNullWhenTriedAddDocumentWithNullValue() {
        String document = repository.addDocument(NEW_KEY, null);
        assertNull(document);
    }

    @Test
    public void returnNullWhenTriedAddDocumentWithEmptyValue() {
        String document = repository.addDocument(NEW_KEY, "");
        assertNull(document);
    }

    @Test
    public void getUniqueKeyByUniqueToken() {
        assertThat(repository.searchDocumentByToken(UNIQUE_VALUE)).containsExactly(UNIQUE_KEY);
    }

    @Test
    public void getKeysByTokenInSeveralDocuments() {
        assertThat(repository.searchDocumentByToken("value3")).containsExactlyInAnyOrder(KEY_2, KEY_3);
        assertThat(repository.searchDocumentByToken(VALUE_1)).containsExactlyInAnyOrder(KEY_1, KEY_2, KEY_3, KEY_5);

    }

    @Test
    public void getKeyByTokenFromBiggerDocument() {
        assertThat(repository.searchDocumentByToken("value5")).containsExactly(KEY_3);
    }

    @Test
    public void returnEmptySetForSearchingWithEmptyToken() {
        assertThat(repository.searchDocumentByToken("")).isEmpty();
    }

    @Test
    public void returnEmptySetForSearchingWithNullToken() {
        assertThat(repository.searchDocumentByToken(null)).isEmpty();
    }

    @Test
    public void returnEmptySetForSearchingWithTokenConsistingFromSpaces() {
        assertThat(repository.searchDocumentByToken("      ")).isEmpty();
    }

    @Test
    public void getKeyByUpperCaseToken() {
        assertThat(repository.searchDocumentByToken("VALUE4")).containsExactly(KEY_3);
    }

    @Test
    public void getEmptyKeyForNonExistingToken() {
        assertThat(repository.searchDocumentByToken("VALUE111")).isEmpty();
    }
}