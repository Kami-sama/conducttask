package com.conduct.service.impl;

import com.conduct.server.repository.DocumentRepository;
import com.conduct.server.service.DocumentService;
import com.conduct.server.service.impl.DocumentServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DocumentServiceImplTest {

    private static final String KEY_1 = "KEY1";
    private static final String KEY_2 = "KEY2";
    private static final String VALUE_1 = "VALUE1";
    private static final String VALUE_2 = "VALUE2";
    private static final String VALUE_3 = "value3";
    private static final String VALUE_4 = "value4";
    private static final String EMPTY_STRING = "";

    @InjectMocks
    private DocumentService documentService = new DocumentServiceImpl();

    @Mock
    private DocumentRepository documentRepository;

    @Before
    public void setUp() throws Exception {
        when(documentRepository.searchDocumentByToken(VALUE_1)).thenReturn(Collections.singleton(KEY_1));
        when(documentRepository.searchDocumentByToken(VALUE_3)).thenReturn(Collections.emptySet());
        when(documentRepository.searchDocumentByToken(VALUE_4)).thenReturn(new HashSet<>(Arrays.asList(KEY_1, KEY_2)));
    }

    @Test
    public void readDocumentFromRepositoryWithKey() {
        when(documentRepository.getDocumentByKey(KEY_1)).thenReturn(VALUE_1);

        assertEquals(VALUE_1, documentService.getDocument(KEY_1).get());
    }

    @Test
    public void returnEmptyWhenReadDocumentWithEmptyKey() {
        when(documentRepository.getDocumentByKey(EMPTY_STRING)).thenReturn(null);

        assertFalse(documentService.getDocument(EMPTY_STRING).isPresent());
    }

    @Test
    public void returnEmptyWhenReadDocumentWithNull() {
        assertFalse(documentService.getDocument(null).isPresent());
    }

    @Test
    public void verifyRepositoryAddDocument() {
        documentService.addDocument(KEY_1, VALUE_1);

        verify(documentRepository).addDocument(KEY_1, VALUE_1);
    }

    @Test
    public void searchKeyByToken() {
        Set<String> keys = documentService.searchDocument(Collections.singletonList(VALUE_1));

        assertThat(keys).containsExactly(KEY_1);
    }

    @Test
    public void searchSeveralKeysByToken() {
        Set<String> keys = documentService.searchDocument(Collections.singletonList(VALUE_4));

        assertThat(keys).containsExactlyInAnyOrder(KEY_1, KEY_2);
    }

    @Test
    public void getEmptyKeysBySearching() {
        Set<String> keys = documentService.searchDocument(Collections.singletonList(VALUE_3));

        assertThat(keys).isEmpty();
    }

    @Test
    public void getEmptyKeysBySearchingWithNullTokens() {
        Set<String> keys = documentService.searchDocument(null);

        assertThat(keys).isEmpty();
    }

    @Test
    public void getEmptyKeysBySearchingWithEmptyTokens() {
        Set<String> keys = documentService.searchDocument(Collections.emptyList());

        assertThat(keys).isEmpty();
    }
}