package com.conduct.server.repository.impl;

import com.conduct.server.repository.DocumentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.stream.Collectors.toSet;

@Repository
public class InMemoryDocumentRepositoryImpl implements DocumentRepository {

    private static final Logger log = LoggerFactory.getLogger(InMemoryDocumentRepositoryImpl.class);

    private Map<String, String> documentCache;

    @PostConstruct
    public void init() {
        documentCache = new ConcurrentHashMap<>(); //todo could be improved with TreeMap
    }


    @Override
    public String getDocumentByKey(String key) {
        log.info("Get document key: '{}'", key);
        if (StringUtils.isEmpty(key)) {
            return null;
        }
        return documentCache.get(key);
    }

    @Override
    public String addDocument(String key, String value) {
        log.info("Add document key:'{}', value:'{}'", key, value);
        if (StringUtils.isEmpty(key) || StringUtils.isEmpty(value)) {
            return null;
        }
        documentCache.put(key, value);
        return value;
    }

    @Override
    public Set<String> searchDocumentByToken(String token) {
        log.info("Search documents by token: '{}'", token);

        if (StringUtils.isEmpty(token) || StringUtils.isEmpty(token.trim())) {
            return Collections.emptySet();
        }

        return documentCache.entrySet().stream()
                .filter(d -> d.getValue().toLowerCase().contains(token.toLowerCase()))
                .map(Map.Entry::getKey)
                .collect(toSet());
    }
}
