package com.conduct.server.repository;

import java.util.Set;

public interface DocumentRepository {
    String getDocumentByKey(String key);

    String addDocument(String key, String value);

    Set<String> searchDocumentByToken(String token);
}
