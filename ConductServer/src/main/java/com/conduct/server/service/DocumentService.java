package com.conduct.server.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface DocumentService {
    Optional<String> getDocument(String key);

    Optional<String> addDocument(String key, String value);

    Set<String> searchDocument(List<String> tokens);
}
