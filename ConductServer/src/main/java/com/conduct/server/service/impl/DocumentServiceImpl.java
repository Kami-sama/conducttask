package com.conduct.server.service.impl;

import com.conduct.server.repository.DocumentRepository;
import com.conduct.server.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private DocumentRepository documentRepository;

    @Override
    public Optional<String> getDocument(String key) {
        return Optional.ofNullable(documentRepository.getDocumentByKey(key));
    }

    @Override
    public Optional<String> addDocument(String key, String value) {
        return Optional.ofNullable(documentRepository.addDocument(key, value));
    }

    @Override
    public Set<String> searchDocument(List<String> tokens) {
        if (tokens == null || tokens.isEmpty()) {
            return Collections.emptySet();
        }

        return tokens.stream()
                .flatMap(t -> documentRepository.searchDocumentByToken(t).stream())
                .collect(Collectors.toSet());
    }
}
