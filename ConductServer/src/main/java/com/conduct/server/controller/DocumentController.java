package com.conduct.server.controller;

import com.conduct.server.service.DocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.util.Collections.singletonMap;

@RestController
@RequestMapping("/document")
public class DocumentController {

    private static final Logger log = LoggerFactory.getLogger(DocumentController.class);

    @Autowired
    private DocumentService documentService;

    @GetMapping("/{key}")
    public ResponseEntity<Optional<String>> getDocument(@PathVariable String key) {
        log.info("Get document with key: '{}'", key);
        Optional<String> document = documentService.getDocument(key);
        if (document.isPresent()) {
            return ResponseEntity.ok(document);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/{key}")
    public ResponseEntity<String> addDocument(@PathVariable String key, @RequestBody String value) {
        log.info("Add document with key: '{}', value: '{}'", key, value);
        Optional<String> document = documentService.addDocument(key, value);
        if (document.isPresent())
            return ResponseEntity.ok(singletonMap(key, value).toString());
        else
            return ResponseEntity.badRequest().body("Key or Value cannot be empty");
    }

    @GetMapping("/search")
    public ResponseEntity<Set<String>> searchDocuments(@RequestParam List<String> tokens) {
        Set<String> keys = documentService.searchDocument(tokens);
        return ResponseEntity.ok(keys);
    }
}
