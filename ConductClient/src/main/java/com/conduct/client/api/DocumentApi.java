package com.conduct.client.api;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonMap;

@Component
public class DocumentApi {

    @Value("${conduct.server.url}")
    private String conductServerUrl;

    @Autowired
    private RestTemplate restTemplate;

    public String getDocumentByKey(String key) {
        String url = conductServerUrl + "/{key}";
        Map<String, String> requestVariable = singletonMap("key", key);

        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class, requestVariable);
        if (response.getStatusCode() == HttpStatus.NOT_FOUND) {
            return "Key's not present in repository";
        }
        return response.getBody();
    }

    public String addDocument(String key, String value) {
        String url = conductServerUrl + "/{key}";
        Map<String, String> requestVariable = singletonMap("key", key);

        ResponseEntity<String> response = restTemplate.postForEntity(url, value, String.class, requestVariable);
        return response.getBody();
    }

    public String searchKeysByTokens(List<String> tokens) {
        String url = conductServerUrl + "/search?tokens={tokens}";
        Map<String, String> requestVariable = singletonMap("tokens", Strings.join(tokens, ','));
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class, requestVariable);
        return response.getBody();
    }
}
