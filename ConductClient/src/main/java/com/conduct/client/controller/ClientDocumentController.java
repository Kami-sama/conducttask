package com.conduct.client.controller;

import com.conduct.client.api.DocumentApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ClientDocumentController { //Name chosen for doen't mix controllers between client and server

    private static final Logger log = LoggerFactory.getLogger(ClientDocumentController.class);

    @Autowired
    private DocumentApi documentApi;

    @GetMapping("/document")
    public String getDocuments(String key) {
        log.info("request document key {}", key);
        return documentApi.getDocumentByKey(key);
    }

    @GetMapping("/addDocument") //Used here GET for quick and easier adding of documents
    public String addDocument(String key, String value) {
        log.info("Add document key: '{}' value: '{}'", key, value);
        return documentApi.addDocument(key, value);
    }

    @GetMapping("/searchKeys")
    public String searchKeys(@RequestParam List<String> tokens) {
        log.info("Tokens for search: '{}'", tokens);
        return documentApi.searchKeysByTokens(tokens);
    }


}
