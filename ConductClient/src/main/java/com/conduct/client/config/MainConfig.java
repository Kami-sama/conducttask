package com.conduct.client.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Configuration
public class MainConfig {
    private static final Logger log = LoggerFactory.getLogger(MainConfig.class);

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder
                .interceptors(getClientHttpRequestInterceptor())
                .errorHandler(getErrorHandler())
                .build();
    }

    private ResponseErrorHandler getErrorHandler() {
        return new ResponseErrorHandler() {

            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return response.getRawStatusCode() >= 500;
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {
                log.error("Response error: {} {}", response.getStatusCode(), response.getStatusText());
            }
        };
    }

    private ClientHttpRequestInterceptor getClientHttpRequestInterceptor() {
        return (request, body, execution) -> {
            log.info("Request performing method: '{}' uri: '{}', body: '{}'", request.getMethod(), request.getURI(), new String(body, "UTF-8"));
            return execution.execute(request, body);
        };
    }
}
